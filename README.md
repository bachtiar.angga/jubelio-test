# JUBELIO BE TEST

## How To Run Locally
- Clone this repo
- Open terminal/command line in cloned folder
- Run
```sh
npm install
```
- Rename .env.example to .env and replace these variables:

| Variable | Info |
| ------ | ------ |
| PORT | Port of Running Server |
| DB_HOST | Database Host |
| DB_NAME | Database Name |
| DB_USER | Database User |
| DB_PORT | Database Port |
| DB_PASS | Database Password |
| ELEVANIA_KEY | Elevania API Key |

- Run
```sh
node index.js
```

# API DOCUMENTATION
Link: https://documenter.getpostman.com/view/5782659/UV5Xgc6s