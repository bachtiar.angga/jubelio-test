const at = require('./trx.controller')

module.exports = (server) => {
    server.route({
        method: "POST",
        path: "/api/trx",
        handler: at.createAdmTrx
    })
    server.route({
        method: "GET",
        path: "/api/trxs",
        handler: at.getAllTrxs
    })
    server.route({
        method: "GET",
        path: "/api/trx/{id}",
        handler: at.findTrx
    })
    server.route({
        method: "DELETE",
        path: "/api/trx/{id}/delete",
        handler: at.deleteTrx
    })
    server.route({
        method: "PUT",
        path: "/api/trx/{id}/update",
        handler: at.updateTrx
    })
}