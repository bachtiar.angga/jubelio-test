const db = require('../../helper/db')

exports.createAdmTrx = async (values, product) => {
    try {
        const pool = await db.pool().connect()
        const prodQuery = db.updateQueryGenerator('products', 'sku', "'" + product.sku + "'", product)
        await pool.query("BEGIN")
        const update = await pool.query(prodQuery.data, [values.sku, product.stock])
        if (update.rowCount == 0) await pool.query('ROLLBACK')
        const create = await pool.query(`INSERT INTO adjustment_transactions(sku,qty,amount) VALUES ($1,$2,$3) RETURNING *`, [values.sku, values.qty, values.amount])
        if (create.rowCount == 0) await pool.query('ROLLBACK')
        await pool.query('COMMIT')
        await pool.release()
        return {
            error: false,
            data: values
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.getAllTrxs = async (queries) => {
    try {
        const pool = await db.pool().connect()
        let query = "SELECT * FROM adjustment_transactions ORDER BY id asc"
        let paginate = ""
        const count = await pool.query(query)
        if (queries.page && queries.limit) {
            const offset = (parseInt(queries.page) - 1) * parseInt(queries.limit)
            paginate = ` LIMIT ${queries.limit} OFFSET ${offset}`
            query = query + paginate
        }
        const res = await pool.query(query)
        await pool.release()
        return {
            error: false,
            data: {
                rows: res.rows,
                total_rows: count.rowCount,
                page: queries.page || "None",
                limit: queries.limit || "None",
                total_page: Math.ceil(count.rowCount / parseInt(queries.limit)) || "None"
            }
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.getTrxById = async (id) => {
    try {
        const pool = await db.pool().connect()
        const trx = await pool.query("SELECT sku,qty,amount FROM adjustment_transactions WHERE id = $1", [id])
        if (trx.rowCount == 0) throw Error("Transaksi tidak ditemukan")
        await pool.release()
        return {
            error: false,
            data: trx.rows[0]
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.updateTrxById = async (id, trx, product, trxVal, prodVal) => {
    try {
        const pool = await db.pool().connect()
        const prodQuery = db.updateQueryGenerator('products', 'sku', "'" + product.sku + "'", product)
        const trxQuery = db.updateQueryGenerator('adjustment_transactions', 'id', id, trx)
        await pool.query("BEGIN")
        const update = await pool.query(prodQuery.data, prodVal)
        if (update.rowCount == 0) await pool.query('ROLLBACK')
        const create = await pool.query(trxQuery.data, trxVal)
        if (create.rowCount == 0) await pool.query('ROLLBACK')
        await pool.query('COMMIT')
        await pool.release()
        return {
            error: false,
            data: trx
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.deleteTrxById = async (id) => {
    try {
        const pool = await db.pool().connect()
        const res = await pool.query("DELETE FROM adjustment_transactions WHERE id = $1", [id])
        if (res.rowCount == 0) throw Error("Gagal menghapus transaksi")
        return {
            error: false,
            data: "Berhasil menghapus transaksi"
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}