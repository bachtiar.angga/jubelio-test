const model = require('./trx.model')
const prodModel = require("../product/product.model")

exports.createAdmTrx = async (r, h) => {
    try {
        const prodObj = {}
        const trxObj = r.payload
        const product = await prodModel.getProductBySku(trxObj.sku)
        if (product.error) throw product.data
        if (product.stock < 1) throw Error("Stok pada produk dipilih sudah habis")
        prodObj.sku = product.data.sku
        prodObj.stock = product.data.stock - parseInt(trxObj.qty)
        trxObj.amount = parseInt(trxObj.qty) * product.data.price
        const { error, data } = await model.createAdmTrx(trxObj, prodObj)
        if (error) throw data
        return h.response({ message: data }).code(201)
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.getAllTrxs = async (r, h) => {
    try {
        const { error, data } = await model.getAllTrxs(r.query)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.deleteTrx = async (r, h) => {
    try {
        const { error, data } = await model.deleteTrxById(r.params.id)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.findTrx = async (r, h) => {
    try {
        const { error, data } = await model.getTrxById(r.params.id)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.updateTrx = async (r, h) => {
    try {
        const prodData = {}
        const trxData = r.payload
        const trx = await model.getTrxById(r.params.id)
        if (trx.error) throw trx.error
        if (r.payload.qty) {
            const product = await prodModel.getProductBySku(trx.data.sku)
            if (product.error) throw product.data
            prodData.stock = (trx.data.qty - parseInt(trxData.qty) + product.data.stock)
            trxData.amount = trxData.qty * product.data.price
            prodData.sku = product.data.sku
        }
        const prodArray = Object.keys(prodData).map((v) => {
            return prodData[v]
        })
        const trxArray = Object.keys(trxData).map((v) => {
            return trxData[v]
        })
        const { error, data } = await model.updateTrxById(r.params.id, trxData, prodData, trxArray, prodArray)
        if (error) throw data
        return h.response({ message: data }).code(200)
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}