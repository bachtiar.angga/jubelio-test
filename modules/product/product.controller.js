const xmljs = require('xml-js')
const axios = require('axios').default
const model = require("./product.model")
const db = require('../../helper/db')

exports.seedProduct = async (r, h) => {
    try {
        const xml = await axios({
            method: "GET",
            url: "http://api.elevenia.co.id/rest/prodservices/product/listing",
            headers: { 'openapikey': process.env.ELEVANIA_KEY }
        })
        const convertToJson = JSON.parse(xmljs.xml2json(xml.data, { compact: true }))
        const { error, data } = await model.seedProduct(convertToJson.Products.product)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ message: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.findAllProducts = async (r, h) => {
    try {
        const { error, data } = await model.getAllProducts(r.query)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.findProduct = async (r, h) => {
    try {
        const { error, data } = await model.getProductById(r.params.id)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.deleteProduct = async (r, h) => {
    try {
        const { error, data } = await model.deleteProductById(r.params.id)
        if (error) {
            throw Error(data)
        } else {
            return h.response({ data: data }).code(200)
        }
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.updateProduct = async (r, h) => {
    try {
        const query = db.updateQueryGenerator('products', 'id', r.params.id, r.payload)
        if (query.error) throw query.data
        const { data, error } = await model.updateProductById(query.data, Object.keys(r.payload).map((v) => {
            return r.payload[v]
        }))
        if (error) throw data
        return h.response({ data: data }).code(200)
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}
exports.createProduct = async (r, h) => {
    try {
        const { data, error } = await model.createProduct(r.payload)
        if (error) throw data
        return h.response({ data: data }).code(201)
    } catch (e) {
        return h.response({ message: e.message }).code(409)
    }
}