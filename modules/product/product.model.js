const db = require('../../helper/db')

exports.seedProduct = async (products) => {
    try {
        let pool = await db.pool().connect()
        let total = 0
        const convertedProducts = products.map((v) => {
            return {
                prod_name: v.prdNm._text,
                sku: v.sellerPrdCd._text,
                description: v.htmlDetail && v.htmlDetail._text,
                stock: Array.isArray(v.ProductOptionDetails) ? v.ProductOptionDetails.reduce((a, b) => a + parseInt(b.stckQty._text), 0) : v.ProductOptionDetails.stckQty._text,
                image: v.prdImage01 && v.prdImage01._text,
                price: v.selPrc._text
            }
        }).reduce((unique, o) => {
            if (!unique.some(obj => obj.sku === o.sku)) {
                unique.push(o)
            }
            return unique
        }, []).map(({
            prod_name,
            sku,
            description,
            stock,
            image,
            price
        }) => { return [prod_name, sku, description, stock, image, price] })
        for (let i = 0; i < convertedProducts.length; i++) {
            let temp = await pool.query("SELECT * FROM products WHERE sku = $1", [convertedProducts[i][1]])
            if (temp.rowCount > 0) continue
            await pool.query("INSERT INTO products(name,sku,description,stock,image,price) VALUES ($1,$2,$3,$4,$5,$6)", convertedProducts[i])
            total = i + 1
        }
        await pool.release()
        return {
            error: false,
            data: `Menambah produk dengan SKU baru sebanyak ${total}`
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.getAllProducts = async (queries) => {
    try {
        const pool = await db.pool().connect()
        let query = "SELECT id,name,sku,image,price,stock FROM products ORDER BY id asc"
        let paginate = ""
        const count = await pool.query(query)
        if (queries.page && queries.limit) {
            const offset = (parseInt(queries.page) - 1) * parseInt(queries.limit)
            paginate = ` LIMIT ${queries.limit} OFFSET ${offset}`
            query = query + paginate
        }
        const res = await pool.query(query)
        await pool.release()
        return {
            error: false,
            data: {
                rows: res.rows,
                total_rows: count.rowCount,
                page: queries.page || "None",
                limit: queries.limit || "None",
                total_page: Math.ceil(count.rowCount / parseInt(queries.limit)) || "None"
            }
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.getProductById = async (id) => {
    try {
        const pool = await db.pool().connect()
        const product = await pool.query("SELECT name,sku,image,price,stock,description FROM products WHERE id = $1", [id])
        if (product.rowCount == 0) throw Error("Product tidak ditemukan")
        await pool.release()
        return {
            error: false,
            data: product.rows[0]
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.deleteProductById = async (id) => {
    try {
        const pool = await db.pool().connect()
        const product = await pool.query("DELETE FROM products WHERE id = $1", [id])
        if (product.rowCount == 0) throw Error("Product tidak ditemukan")
        await pool.release()
        return {
            error: false,
            data: "Berhasil menghapus product"
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.updateProductById = async (query, values) => {
    try {
        const pool = await db.pool().connect()
        const res = await pool.query(query, values)
        await pool.release()
        if (res.rowCount == 0) throw Error("Product gagal diperbarui")
        return {
            error: false,
            data: res.rows[0]
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.createProduct = async (values) => {
    try {
        const pool = await db.pool().connect()
        const res = await pool.query("INSERT INTO products(name,sku,image,price,description) VALUES ($1,$2,$3,$4,$5) RETURNING *", [values.name, values.sku, values.image, values.price, values.description])
        await pool.release()
        return {
            error: false,
            data: res.rows[0]
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}
exports.getProductBySku = async (sku) => {
    try {
        const pool = await db.pool().connect()
        const product = await pool.query("SELECT * FROM products WHERE sku = $1", [sku])
        if (product.rowCount == 0) throw Error("Product tidak ditemukan")
        await pool.release()
        return {
            error: false,
            data: product.rows[0]
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}