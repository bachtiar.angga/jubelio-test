const product = require('./product.controller')

module.exports = (server) => {
    server.route({
        method: "GET",
        path: "/api/product/seeding",
        handler: product.seedProduct
    })
    server.route({
        method: "GET",
        path: "/api/products",
        handler: product.findAllProducts
    })
    server.route({
        method: "GET",
        path: "/api/product/{id}",
        handler: product.findProduct
    })
    server.route({
        method: "DELETE",
        path: "/api/product/{id}/delete",
        handler: product.deleteProduct
    })
    server.route({
        method: "PUT",
        path: "/api/product/{id}/update",
        handler: product.updateProduct
    })
    server.route({
        method: "POST",
        path: "/api/product",
        handler: product.createProduct
    })
}