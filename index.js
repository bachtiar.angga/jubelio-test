const hapi = require('@hapi/hapi')
const db = require('./helper/db')

const productRoute = require('./modules/product/product.route')
const trxRoute = require('./modules/transaction/trx.route')

require("dotenv").config()

const init = async () => {
    const server = hapi.Server({
        port: process.env.PORT,
        host: 'localhost'
    })
    const pool = await db.pool().connect()
    await pool.query(`CREATE TABLE IF NOT EXISTS products (
        id SERIAL PRIMARY KEY,
        name VARCHAR(100) NOT NULL,
        sku VARCHAR(80) UNIQUE NOT NULL,
        description TEXT,
        image TEXT,
        price NUMERIC(15,2),
        stock INTEGER
    );
    CREATE TABLE IF NOT EXISTS adjustment_transactions (
        id SERIAL PRIMARY KEY,
        sku VARCHAR(80) NOT NULL,
        qty INTEGER NOT NULL,
        amount NUMERIC(15,2) NOT NULL,
        CONSTRAINT fk_product_sku FOREIGN KEY (sku) REFERENCES products (sku) ON DELETE CASCADE
    );`)
    await pool.release()
    server.route({
        method: 'GET',
        path: '/',
        handler: () => {
            return { "message": "Hello, it's me" }
        }
    })
    productRoute(server)
    trxRoute(server)
    await server.start()
    console.log(`Run on localhost:${process.env.PORT}`)
}

process.on('unhandledRejection', (err) => {
    console.error(err)
    process.exit(1)
})

init()