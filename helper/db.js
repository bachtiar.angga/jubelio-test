const pg = require('pg').Pool

exports.pool = () => {
    return new pg({
        user: process.env.DB_USER,
        host: process.env.DB_HOST,
        database: process.env.DB_NAME,
        password: process.env.DB_PASS,
        port: process.env.DB_PORT
    })
}
exports.updateQueryGenerator = (table, param, paramValue, body) => {
    try {
        let query = [`UPDATE ${table}`]
        query.push('SET')
        const set = []
        Object.keys(body).forEach((key, i) => {
            set.push(key + ' = ($' + (i + 1) + ')')
        })
        query.push(set.join(', '))
        query.push(`WHERE ${param} = ${paramValue}`)
        return {
            error: false,
            data: query.join(' ')
        }
    } catch (e) {
        return {
            error: true,
            data: e
        }
    }
}